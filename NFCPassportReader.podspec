Pod::Spec.new do |spec|

  spec.name         = "NFCPassportReader"
  spec.version      = "1.0.11"
  spec.summary      = "This package handles reading an NFC Enabled passport using iOS 13 CoreNFC APIS"

  spec.homepage     = "https://bitbucket.org/lavatech/nfcpassport-reader.git"
  spec.license      = "MIT"
  spec.author       = { "Arthur Lavatech" => "arthur@lavatech.ltd.uk" }
  spec.platform = :ios
  spec.ios.deployment_target = "13.0"

  spec.source       = { :git => "https://bitbucket.org/lavatech/nfcpassport-reader.git", :tag => "#{spec.version}" }

  spec.source_files  = "Sources/**/*.{swift}"

  spec.swift_version = "5.0"

  spec.dependency "OpenSSL-Universal", '1.1.2300'
  spec.xcconfig          = { 'OTHER_LDFLAGS' => '-weak_framework CryptoKit -weak_framework CoreNFC',
                             'ENABLE_BITCODE' => '"NO' }
end
